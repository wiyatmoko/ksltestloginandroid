package com.babsnet.kslwiyatmoko.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.babsnet.kslwiyatmoko.data.LoginResponse
import com.babsnet.kslwiyatmoko.data.NetworkResult
import com.babsnet.kslwiyatmoko.data.User
import com.babsnet.kslwiyatmoko.data.pref.SavePreference
import com.babsnet.kslwiyatmoko.repository.Repository
import kotlinx.coroutines.launch
import retrofit2.Response


class LoginViewModel(
    private val repository: Repository,
    var savePreference: SavePreference
) : ViewModel(){

    var loginResponse: MutableLiveData<NetworkResult<LoginResponse>> = MutableLiveData()

    @SuppressLint("SuspiciousIndentation")
    fun doLogin(user: User) = viewModelScope.launch {
        loginResponse.value = NetworkResult.Loading()
            try {
                val response = repository.remote.postLogin(user)
                loginResponse.value = handleLoginResponse(response)
            } catch (e: Exception) {
                loginResponse.value = NetworkResult.Error("Login Error")
            }
        }

    private fun handleLoginResponse(response: Response<LoginResponse>): NetworkResult<LoginResponse>? {
        when {

            response.code() == 400 -> {
                return NetworkResult.Error("Login Error")
            }
            response.isSuccessful -> {
                val loginResponse = response.body()
                return NetworkResult.Success(loginResponse!!)
            }
            else -> {
                return NetworkResult.Error(response.message())
            }
        }

    }

    fun updateSessionUser(token: String) {
        return savePreference.updateToken(token)
    }

    fun clearUserSession() {
        return savePreference.clearUserPreferences()
    }




}