package com.babsnet.kslwiyatmoko.di

import android.content.Context
import com.babsnet.kslwiyatmoko.data.pref.SavePreference
import com.babsnet.kslwiyatmoko.network.LoginApiInterface
import com.babsnet.kslwiyatmoko.repository.RemoteDataSource
import com.babsnet.kslwiyatmoko.repository.Repository
import com.babsnet.kslwiyatmoko.utils.Constanst.BASE_URL
import com.babsnet.kslwiyatmoko.viewmodel.LoginViewModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    single(named("BASE_URL")){
        BASE_URL
    }

    single {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        interceptor
    }

    single {
        val client = OkHttpClient().newBuilder()
            .connectTimeout(30,TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)

        if(org.koin.android.BuildConfig.DEBUG){
            client.addInterceptor(get<HttpLoggingInterceptor>())
        }
        client.build()
    }

    single {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single {
        Retrofit.Builder().baseUrl(get<String>(named("BASE_URL")))
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .client(get())
            .build()
    }

    single {
        get<Retrofit>().create(LoginApiInterface::class.java)
    }
}

val appModule = module {
    viewModel {
        LoginViewModel(get(), get())
    }
}

val repoModule = module {
    single {
        Repository(get())

    }
}

val remoteModule = module {
    single {
        RemoteDataSource(get())
    }
}

val sharedPreferences = module {
    factory { key ->
        androidContext().getSharedPreferences(key.get<String>(0), Context.MODE_PRIVATE)
    }
    factory {
        SavePreference()
    }
}



