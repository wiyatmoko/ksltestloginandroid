package com.babsnet.kslwiyatmoko.view


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.AlertDialog
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.babsnet.kslwiyatmoko.R
import com.babsnet.kslwiyatmoko.navigation.Screen
import com.babsnet.kslwiyatmoko.viewmodel.LoginViewModel
import org.koin.androidx.compose.getViewModel


@Composable
fun HomeScreen(navController: NavHostController){
    ScaffoldWithTopBar(navController)
}


@Composable
fun ScaffoldWithTopBar(navController: NavHostController) {
    val mContext = LocalContext.current
    val viewModel = getViewModel<LoginViewModel>()
    val showDialog = remember { mutableStateOf(false) }
    if (showDialog.value) {
        AlertDialogToPageLogin(navController, showDialog, viewModel)
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(text = "User Info")
                },
                navigationIcon = {
                    IconButton(onClick = {
                        showDialog.value = true
                    }) {

                        Icon(
                            Icons.Filled.ArrowBack, "backIcon")
                    }
                },
                backgroundColor = MaterialTheme.colors.primary,
                contentColor = Color.White,
                elevation = 10.dp
            )
        }, content = {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color(R.color.white)),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "Welcome, to page user information",
                    fontSize = 24.sp,
                    color = Color.White,
                )
            }

        })
}

@Composable
fun AlertDialogToPageLogin(
    navController: NavHostController,
    showDialog: MutableState<Boolean>,
    viewModel: LoginViewModel
) {
    AlertDialog(
        onDismissRequest = { },
        confirmButton = {
            TextButton(onClick = {
                viewModel.clearUserSession()
                navController.navigate(Screen.Login.route)
            })
            { Text(text = "OK") }
        },
        dismissButton = {
            TextButton(onClick = {
                showDialog.value = false
            })
            { Text(text = "Cancel") }
        },
        title = { Text(text = "Please confirm") },
        text = { Text(text = "Should I continue with the requested action?") }
    )
}