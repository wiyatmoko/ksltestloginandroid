package com.babsnet.kslwiyatmoko.view

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import com.babsnet.kslwiyatmoko.R
import com.babsnet.kslwiyatmoko.data.UserSession
import com.babsnet.kslwiyatmoko.navigation.Screen
import com.babsnet.kslwiyatmoko.viewmodel.LoginViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun SplashScreen(
    navController: NavHostController
) {
    val viewModel = getViewModel<LoginViewModel>()
    Splash()
    navController.popBackStack()
    viewModel.savePreference.reloadUserSession()
    if (UserSession.token.isEmpty()) {
        navController.navigate(Screen.Login.route)
    } else {
        navController.navigate(Screen.Home.route)
    }
}

@Composable
fun Splash() {
    val infiniteTransition = rememberInfiniteTransition()
    val color by infiniteTransition.animateColor(
        initialValue = Color.Blue,
        targetValue = Color.Green,
        animationSpec = infiniteRepeatable(
            animation = tween(10000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )
    Box(
        modifier = Modifier
            .background(color)
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.logo),
            contentDescription = "KSL"
        )
    }
}

@Composable
@Preview
fun SplashScreenPreview() {
    Splash()
}