package com.babsnet.kslwiyatmoko.view

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.babsnet.kslwiyatmoko.R
import com.babsnet.kslwiyatmoko.data.NetworkResult
import com.babsnet.kslwiyatmoko.data.User
import com.babsnet.kslwiyatmoko.navigation.Screen
import com.babsnet.kslwiyatmoko.viewmodel.LoginViewModel
import org.koin.androidx.compose.getViewModel

@Composable
fun LoginScreen(navController: NavHostController) {
    Login(navController)
}


@Composable
fun Login(navController: NavHostController) {
    val textValueEmail = remember { mutableStateOf("") }
    val textValuePassword = remember {
        mutableStateOf("")
    }
    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        TextField(textValueEmail, textValuePassword)
        ButtonLogin(navController, textValueEmail, textValuePassword)
    }
}


@Composable
fun TextField(textValueEmail: MutableState<String>, textValuePassword: MutableState<String>) {


    val passwordVisible = remember { mutableStateOf(false) }
    val primaryColor = colorResource(id = R.color.black)

    OutlinedTextField(
        label = { Text(text = "Email") },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = primaryColor,
            focusedLabelColor = primaryColor,
            cursorColor = primaryColor
        ),
        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Email),
        value = textValueEmail.value,
        onValueChange = {
            textValueEmail.value = it
        },

    )

    OutlinedTextField(
        label = { Text(text = "Password") },
        colors = TextFieldDefaults.outlinedTextFieldColors(
            focusedBorderColor = primaryColor,
            focusedLabelColor = primaryColor,
            cursorColor = primaryColor
        ),
        visualTransformation = if (passwordVisible.value) VisualTransformation.None else PasswordVisualTransformation(),
        keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Password),
        value = textValuePassword.value,
        onValueChange = {
            textValuePassword.value = it
        },

        trailingIcon = {
            val image = if (passwordVisible.value)
                Icons.Filled.Visibility
            else Icons.Filled.VisibilityOff

            // Localized description for accessibility services
            val description = if (passwordVisible.value) "Hide password" else "Show password"

            // Toggle button to hide or display password
            IconButton(onClick = {passwordVisible.value = !passwordVisible.value}){
                Icon(imageVector  = image, description)
            }
        }


    )

}

@Composable
fun ButtonLogin(
    navController: NavHostController,
    textValueEmail: MutableState<String>,
    textValuePassword: MutableState<String>
) {
    val viewModel = getViewModel<LoginViewModel>()
    val mContext = LocalContext.current
    Button(
        onClick = {
            val loginData = User(textValueEmail.value, textValuePassword.value)
            viewModel.doLogin(loginData)

        },
        colors = ButtonDefaults.buttonColors(backgroundColor = colorResource(id = R.color.black)),
        border = BorderStroke(
            1.dp,
            color = colorResource(id = R.color.black)
        )

    ) {
        Text(
            text = "Login",
            color = Color.White,
            style = TextStyle(
                fontSize = 16.sp
            )
        )
    }
    val loginResponse  = viewModel.loginResponse.observeAsState()
    when(loginResponse.value){
        is NetworkResult.Error<*> -> {
            Toast.makeText(mContext, "Login Error", Toast.LENGTH_SHORT).show()
        }

        is NetworkResult.Success<*> -> {
            loginResponse.value?.data?.token?.let { Log.d("ceklah", it) }
            if(loginResponse.value?.data?.token?.isNotEmpty() == true){
                viewModel.updateSessionUser(loginResponse.value?.data?.token!!)
                navController.navigate(Screen.Home.route){
                    launchSingleTop = true
                }
            }
        }
    }
}
@Composable
@Preview
fun LoginScreenPreview(){
    val navController: NavHostController? = null
    Login(navController!!)
}


