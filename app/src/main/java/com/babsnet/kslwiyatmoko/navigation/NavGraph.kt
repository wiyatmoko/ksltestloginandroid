package com.babsnet.kslwiyatmoko.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.babsnet.kslwiyatmoko.view.HomeScreen
import com.babsnet.kslwiyatmoko.view.LoginScreen
import com.babsnet.kslwiyatmoko.view.SplashScreen


@Composable
fun SetupNavGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.Splash.route,
    ) {
        composable(route= Screen.Home.route){
            HomeScreen(navController = navController)
        }
        composable(route = Screen.Splash.route) {

           SplashScreen(navController = navController)
        }

        composable(route = Screen.Login.route){
            LoginScreen(navController = navController)
        }
        

    }
}


