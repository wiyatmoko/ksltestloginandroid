package com.babsnet.kslwiyatmoko.navigation

sealed class Screen(val route: String){
    object Splash: Screen("splash_screen")
    object Login: Screen("login_screen")
    object Home: Screen("home_screen")
}
