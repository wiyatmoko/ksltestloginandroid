package com.babsnet.kslwiyatmoko

import android.app.Application
import com.babsnet.kslwiyatmoko.di.appModule
import com.babsnet.kslwiyatmoko.di.networkModule
import com.babsnet.kslwiyatmoko.di.remoteModule
import com.babsnet.kslwiyatmoko.di.repoModule
import com.babsnet.kslwiyatmoko.di.sharedPreferences
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin


class KslWiyatmoko : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@KslWiyatmoko)
            modules(listOf(networkModule, appModule, repoModule, remoteModule,sharedPreferences
            ))
        }
    }
}