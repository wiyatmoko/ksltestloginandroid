package com.babsnet.kslwiyatmoko.network

import com.babsnet.kslwiyatmoko.data.LoginResponse
import com.babsnet.kslwiyatmoko.data.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApiInterface {

    @POST("/api/login")
    suspend fun doLogin(@Body user: User): Response<LoginResponse>

}