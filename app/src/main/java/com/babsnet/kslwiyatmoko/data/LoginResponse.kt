package com.babsnet.kslwiyatmoko.data

import com.google.gson.annotations.SerializedName


class LoginResponse(
    @SerializedName("token")
    val token: String
)
