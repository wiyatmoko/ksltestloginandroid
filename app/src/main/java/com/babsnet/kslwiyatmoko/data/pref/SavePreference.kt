package com.babsnet.kslwiyatmoko.data.pref

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.babsnet.kslwiyatmoko.data.UserSession
import com.babsnet.kslwiyatmoko.utils.Constanst.PREFERENCES_TOKEN
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.parameter.parametersOf

class SavePreference : KoinComponent {
    private val sharedPreferences by inject<SharedPreferences> {
        parametersOf("userPref")
    }

    @SuppressLint("CommitPrefEdits")
    fun updateToken(token: String,) {
        val editor = sharedPreferences.edit()
        editor.putString(PREFERENCES_TOKEN, token)
        editor.apply()
    }

    fun reloadUserSession(){
        UserSession.token = sharedPreferences.getString(PREFERENCES_TOKEN, "").toString()
    }

    fun clearUserPreferences() {
        UserSession.clearValues()
        sharedPreferences.edit().clear().apply()
    }
}