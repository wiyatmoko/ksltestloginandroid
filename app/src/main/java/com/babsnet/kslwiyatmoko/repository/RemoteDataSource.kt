package com.babsnet.kslwiyatmoko.repository

import com.babsnet.kslwiyatmoko.data.LoginResponse
import com.babsnet.kslwiyatmoko.data.User
import com.babsnet.kslwiyatmoko.network.LoginApiInterface
import retrofit2.Response


class RemoteDataSource  (
    private val loginApiInterface: LoginApiInterface
){
    suspend fun postLogin(user: User): Response<LoginResponse> {
        return loginApiInterface.doLogin(user)
    }

}